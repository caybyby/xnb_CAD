#include <BRepPrimAPI_MakeBox.hxx>

#include <IVtkTools_ShapeDataSource.hxx>

#include <vtkAutoInit.h>
#include <vtkRenderer.h>
#include <VtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkPolyDataMapper.h>

VTK_MODULE_INIT(vtkRenderingOpenGL2)
VTK_MODULE_INIT(vtkInteractionStyle)
int main()
{
  BRepPrimAPI_MakeBox box(2, 2, 2);
  const TopoDS_Shape& shape = box.Shape();

  vtkNew<vtkRenderWindow> renderWindow; //创建一个vtk窗口
  vtkNew<vtkRenderer> render; //创建一个vtk渲染器
  renderWindow->AddRenderer(render); //在窗口中加入渲染器

  vtkNew<vtkRenderWindowInteractor> iren; //创建一个vtk交互器
  vtkNew<vtkInteractorStyleTrackballCamera> istyle; //创建vtk相机交互器样式

  iren->SetRenderWindow(renderWindow); //设置渲染窗口
  iren->SetInteractorStyle(istyle); //设置交互器样式

  vtkNew<IVtkTools_ShapeDataSource> occSource; //创建一个可以被VTK使用的OCC数据源
  occSource->SetShape(new IVtkOCC_Shape(shape)); //将shape添加到数据源中

  vtkNew<vtkPolyDataMapper> mapper; //创建一个VTK数据类型

  mapper->SetInputConnection(occSource->GetOutputPort()); //创建一个管道，将occ数据导入到VTK数据中

  vtkNew<vtkActor> actor; //创建一个vtk actor
  actor->SetMapper(mapper); //将vtk数据交给actor
  render->AddActor(actor); //在渲染器中加入vtk actor

  iren->Initialize(); //初始化交互器
  iren->Start(); //开始运行交互器

  return 0;
}

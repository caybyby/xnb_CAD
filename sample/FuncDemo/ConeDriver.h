#ifndef CONEDRIVER_H
#define CONEDRIVER_H


#include "BaseDriver.h"

#include <Standard_DefineHandle.hxx>
#include <TFunction_Logbook.hxx>

DEFINE_STANDARD_HANDLE(ConeDriver, BaseDriver)

// A Cone function driver.
class ConeDriver : public BaseDriver
{
public:

    // ID of the function driver
    static const Standard_GUID& GetID();

    // Constructor
        ConeDriver();

        // Execution.
        virtual Standard_Integer Execute(Handle(TFunction_Logbook)& log) const;

        DEFINE_STANDARD_RTTIEXT(ConeDriver, BaseDriver)
};

#endif // CONEDRIVER_H

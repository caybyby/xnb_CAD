#ifndef CIRCLEDRIVER_H
#define CIRCLEDRIVER_H


#include "BaseDriver.h"

#include <Standard_DefineHandle.hxx>
#include <TFunction_Logbook.hxx>

DEFINE_STANDARD_HANDLE(CircleDriver, BaseDriver)

// A Circle function driver.
class CircleDriver : public BaseDriver
{
public:

    // ID of the function driver
    static const Standard_GUID& GetID();

    // Constructor
        CircleDriver();

        // Execution.
        virtual Standard_Integer Execute(Handle(TFunction_Logbook)& log) const;

        DEFINE_STANDARD_RTTIEXT(CircleDriver, BaseDriver)
};
#endif // CIRCLEDRIVER_H

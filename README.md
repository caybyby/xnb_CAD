# Open CASCADE Technology 7.8.0

## 基类

### 数据类型 Standard_XXX

- 原始类，例如：布尔、字符、整数、实数等类
- 字符串类，处理 Unicode
- 集合类，包含数组、列表、队列等集合类
- 常用数学工具，数值算法和线性代数计算
- 基础类型，例如颜色、时间
- 原始几何
- 异常

### 以上类型包含服务

- 智能指针, Handle
- 内存管理
- 运行时类型信息(RTTI)
- 封装 C++ 流
- 表达式通用定义，自定义脚本
- 配置资源文件和自定义信息文件
- 进度指示和用户中断

## 建模数据

### 边界表示(BRep)

- 描述点、向量、曲线和表面
- 曲线和表面插值和近似
- 创建
- NURBS曲线和表面转换
- 采样曲线上点的坐标
- 计算几何之间的极值

### 简单几何

- Vertex
- Edge
- Wire
- Face
- Shell
- Solid
- Composite solid
- Compound

### 建模算法

#### Low-Level

##### 几何工具

- 计算几何图形的交点
- 投影
- 约束线段和圆的生成
- 构建曲线和曲面

##### 拓扑工具

- 细分形状
- 检查形状正确定义
- 确定形状的本地和全局属性(例如导数和质量惯性)
- 仿射变换
- 找平面中的边
- 转换位NURBS
- 缝合和分离拓扑元素

#### High-Level

- 基本体素
- 布尔操作

### 网格

- 三角细分

### 显示

#### 关键工具集

- TKV3d(high-level Api) 对象交互
- TKService(low-level Api) 从体素数组进行管理和创建进行渲染
- TKOpenGl 实现图形驱动

### 数据交换 允许与其他CAD系统交互

#### 支持格式

- STEP
- IGES
- glTF
- OBJ
- VRML
- STL

### 形状修复

- 分析形状的拓扑结构
- 修复错误形状
- 升级和改变形状特性

### 应用程序框架 OCAF

---

## 编译OCC

环境：

- win11
- msvc2017_64 VS2022
- Qt5
- CMake-gui

下载地址&emsp;<https://dev.opencascade.org/release>

在项目目录下创建3rdparty目录，下载第三方组件 *(注意区分32bit 和 64bit)*, 并解压到该目录下

- FFmpeg
- Freeimage
- Freetype
- Tck/Tk

在项目目录下创建build目录, CMake-gui打开源代码目录和构建目录，点击Configure

![CMake-gui](/screenshot/ss01.png)

**BUILD**    勾选 **BUILD_SAMPLES_QT**
**INSTALL**  选择安装目录
**USE**      勾选 **USE_FFMPEG/USE_FREEIMAGE**, 最好还是勾选USE_VTK，才能编译出VTK相关的接口工具文件
**Qt5_DIR**  选择 **/Qt5.XXX/5.XXX/msvc2017_64/lib/cmake/Qt5**

Configure -> Generate -> OpenProject ->VS生成解决方案

下载VTK9.3.0&emsp;<https://vtk.org/download/>，并用CMake-gui生成项目并安装，原理同上

VTK9.3.0 编译出现问题<https://blog.csdn.net/ruihaha/article/details/134502360>
**注意**
![VTK9.3.0](/screenshot/ss04.png)

### Qt5 msvc2017_64 项目构建

CMake出错&emsp;<https://forum.qt.io/topic/120275/cannot-build-with-cmake-qtcreator-and-msvc-2019/10>

Qt5带来的坑，默认添加 \<unsupported>
![CMake-Qt5](/screenshot/ss02.png)

![CMake-Qt5](/screenshot/ss03.png)

构建项目成功，CMake执行通过，但是出现**程序异常退出**，原因是连接不到库文件，需要在环境变量中添加Path

![CMake-Qt5](/screenshot/ss05.png)

Path添加：
%THIRDPARTY_DIR%\ffmpeg-3.3.4-64\bin;%THIRDPARTY_DIR%\ffmpeg-3.3.4-64\lib;%THIRDPARTY_DIR%\freeimage-3.17.0-vc14-64\bin;%THIRDPARTY_DIR%\freeimage-3.17.0-vc14-64\lib;%THIRDPARTY_DIR%\freetype-2.5.5-vc14-64\bin;%THIRDPARTY_DIR%\freetype-2.5.5-vc14-64\lib;%THIRDPARTY_DIR%\tcltk-86-64\bin;%THIRDPARTY_DIR%\tcltk-86-64\lib;%THIRDPARTY_DIR%\VTK\bin;%THIRDPARTY_DIR%\VTK\lib;%CASROOT%\win64\vc14\libd%CASROOT%\win64\vc14\bind;

### 到此编译运行成功

![CMake-Qt5](/screenshot/ss06.png)

